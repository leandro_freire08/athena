package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type MagentoApi struct {
	MagentoAdminUser string
	MagentoAdminPass string
	MagentoApi       string
}

func (m *MagentoApi) RequestToken() (token []byte) {

	apiUrl := m.MagentoApi
	resource := "/integration/admin/token"

	client := &http.Client{}

	values := map[string]string{"username": m.MagentoAdminUser, "password": m.MagentoAdminPass}
	jsonValue, _ := json.Marshal(values)

	req, _ := http.NewRequest("POST", fmt.Sprintf(apiUrl, resource), bytes.NewBuffer(jsonValue))
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	r, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	return r[1 : len(r)-1] // Remove quotes from token string
}
