package main

import (
	. "config"
	. "dao"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	. "models"
	"net/http"
	. "service"

	"gopkg.in/mgo.v2/bson"
)

var config = Config{}
var dao = ProductsDAO{}
var magentoApi = MagentoApi{}

// Parse the configuration file 'config.toml', and establish a connection to DB
func init() {
	config.Read()

	dao.Server = config.MongoServer
	dao.Database = config.MongoDatabase
	dao.Connect()

	magentoApi.MagentoAdminUser = config.MagentoAdminUser
	magentoApi.MagentoAdminPass = config.MagentoAdminPass
	magentoApi.MagentoApi = config.MagentoApi
}

func main() {

	magentoToken := magentoApi.RequestToken()

	searchCriteria := "searchCriteria[filter_groups][0][filters][0][field]=type_id&searchCriteria[filter_groups][0][filters][0][value]=simple&searchCriteria[filter_groups][0][filters][0][conditionType]=like&searchCriteria[pageSize]=10"

	url := fmt.Sprintf("http://127.0.0.1/rest/V1/products/?%s", searchCriteria)

	// For control over HTTP client headers,
	// redirect policy, and other settings,
	// create a Client
	// A Client is an HTTP client
	client := http.Client{}

	// Build the request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal("NewRequest: ", err)
		fmt.Println(err)
		return
	}

	token := fmt.Sprintf("Bearer %s", magentoToken)
	req.Header.Add("Authorization", token)

	// Send the request via a client
	// Do sends an HTTP request and
	// returns an HTTP response
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
		fmt.Println(err)
		return
	}

	// Callers should close resp.Body
	// when done reading from it
	// Defer the closing of the body
	defer resp.Body.Close()

	// Read all the response body
	rb, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("Fail to read response: ", err)
		fmt.Println(err)
		return
	}
	// Fill the product with the data from the JSON
	var mageProduct MagentoProduct
	json.Unmarshal(rb, &mageProduct)

	var products []Product

	// Loop over array and convert Items to Go struct
	for _, p := range mageProduct.Items {
		products = append(products, p)
	}

	// Loop over Go struct products and insert into Mongo
	for _, product := range products {
		product.ID = bson.NewObjectId()
		if err := dao.Insert(product); err != nil {
			log.Fatal("Unable to insert into MongoDB: ", err)
			return
		}
		fmt.Println("Sku: ", product.Sku, " Inserted!")
	}
}

// In order to know how many products exists in source (Magento)
func buildInitialSearchCriteria() (searchCriteria string) {
	searchCriteria = "searchCriteria[pageSize]=1&searchCriteria[currentPage]=1"
	return searchCriteria
}

func buildSearchCriteriaForProducts() {
	//TODO
}
