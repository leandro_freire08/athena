package models

import "gopkg.in/mgo.v2/bson"

type Product struct {
	ID 				 bson.ObjectId `bson:"_id"`
	ProductID        int           `json:"id"`
	Sku              string        `bson:"sku" json:"sku"`
	Name             string        `bson:"name" json:"name"`
	AttributeSetID   int           `bson:"attribute_set_id" json:"attribute_set_id"`
	Price            int           `bson:"price" json:"price"`
	Status           int           `bson:"status" json:"status"`
	Visibility       int           `bson:"visibility" json:"visibility"`
	TypeID           string        `bson:"type_id" json:"type_id"`
	CreatedAt        string        `bson:"created_at" json:"created_at"`
	UpdatedAt        string        `bson:"updated_at" json:"updated_at"`
	ProductLinks     []struct{} `bson:"product_links" json:"product_links"`
	TierPrices       []struct{} `bson:"tier_prices" json:"tier_prices"`
	CustomAttributes []struct {
		AttributeCode string `bson:"attribute_code" json:"attribute_code"`
		Value         string `bson:"value" json:"value"`
	} `bson:"custom_attributes" json:"custom_attributes"`
}